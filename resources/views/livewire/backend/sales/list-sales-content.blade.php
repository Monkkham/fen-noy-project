<div wire:poll>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-6">
                    <h6><i class="fas fa-store"></i> ຂາຍໃຫ້ລູກຄ້າ <i class="fa fa-angle-double-right"></i>
                        ລາຍການຂາຍໃຫ້ລູກຄ້າ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">ໜ້າຫຼັກ</a>
                        </li>
                        <li class="breadcrumb-item active">ລາຍການຂາຍໃຫ້ລູກຄ້າ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    @foreach ($function_available as $item1)
    @if ($item1->function->name == 'action_17')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="start_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="end_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <input wire:model.live="search" type="text" class="form-control"
                                        placeholder="ຄົ້ນຫາ...">
                                </div>
                                {{-- <div class="col-md-2">
                                    <div class="form-group">
                                        <select wire:model="status" class="form-control">
                                            <option value="" selected>ເລືອກ-ສະຖານະ</option>
                                            <option value="1">ລໍຖ້າຮັບ</option>
                                            <option value="2">ນຳເຂົ້າສຳເລັດ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select wire:model="status" class="form-control">
                                            <option value="" selected>ເລືອກ-ຊຳລະ</option>
                                            <option value="1">ຄ້າງຊຳລະ</option>
                                            <option value="2">ຊຳລະເເລ້ວ</option>
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-md-5"></div>
                                <div class="col-md-1">
                                    {{-- <a href="{{route('backend.order_add')}}" type="button" class="btn btn-warning" style="width: auto;"><i class="fa fa-cart-plus"></i> {{__('lang.purchase_orders')}}</a> --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered text-center table-striped text-sm">
                                    <thead>
                                        <tr class="text-center bg-light">
                                            <th>ລຳດັບ</th>
                                            <th>ລະຫັດ</th>
                                            <th>ວັນທີ</th>
                                            <th>ລູກຄ້າ</th>
                                            <th>ຍອດລວມ</th>
                                            {{-- <th>ຍອດຊຳລະ</th>
                                            <th>ຍອດຫນີ້</th> --}}
                                            <th>ປະເພດ</th>
                                            <th>ສະຖານະ</th>
                                            <th>ຜູ້ສ້າງ</th>
                                            <th>ຈັດການ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{ $num++ }}</td>
                                                <td>{{ $item->code }}</td>
                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                                <td>
                                                    @if (!empty($item->customer))
                                                        {{ $item->customer->name_lastname }} <br>
                                                        {{ $item->customer->phone }}
                                                    @endif
                                                </td>
                                                <td class="text-primary">{{ number_format($item->sales_detail_sum_subtotal) }}
                                                    ₭</td>
                                                {{-- <td class="text-success">
                                                    {{ number_format($item->orders_logs_sum_total_paid) }}
                                                    ₭</td>
                                                <td class="text-danger">
                                                    {{ number_format($item->total - $item->orders_logs_sum_total_paid) }}
                                                    ₭
                                                </td> --}}
                                                <td>
                                                    @if ($item->type == '1')
                                                        <p class="text-success"><i class="fas fa-hand-holding-usd"></i>
                                                            ເງິນສົດ</p>
                                                    @elseif($item->type == '2')
                                                            <button class="btn btn-outline-danger btn-sm" wire:click='showOnepay({{ $item->id }})'><i class="fas fa-money"></i> ເງິນໂອນ</button>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->status == '3')
                                                        <p class="text-success"><i class="fas fa-check-circle"></i>
                                                            ຂາຍສຳເລັດ</p>
                                                    @elseif($item->status == '2')
                                                    
                                                        <p class="text-success"><i class="fas fa-check-circle"></i>
                                                            ເງິນໂອນ</p>
                                                    @elseif($item->status == '3')
                                                        <p class="text-danger"><i class="fas fa-times-circle"></i>
                                                            ຍົກເລີກ</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (!empty($item->employee))
                                                        {{ $item->employee->name_lastname }}
                                                    @endif
                                                </td>
                                                <td width="2%" style="text-align: center">
                                                    <div wire:ignore class="btn-group btn-left">
                                                        <button wire:click='ShowBill({{ $item->id }})' type="button" class="btn btn-info btn-sm"><i
                                                                class="fas fa-print"></i>
                                                        </button>
                                                        <button wire:click='ShowUpdate({{ $item->id }})' type="button" class="btn btn-warning btn-sm"> <i
                                                                class="fas fa-pen"></i>
                                                        </button>
                                                        <button wire:click='showDestory({{ $item->id }})' type="button" class="btn btn-danger btn-sm"> <i
                                                                class="fas fa-trash"></i>
                                                        </button>
                                                        <div class="dropdown-menu" role="menu">
                                                            @if ($item->status != '2')
                                                                {{-- @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_37') --}}
                                                                <a class="dropdown-item" href="javascript:void(0)"
                                                                    wire:click="ShowConfirm({{ $item->id }})"><i
                                                                        class="fas fa-file-import">
                                                                    </i> ນຳເຂົ້າສາງ</a>
                                                            @endif
                                                            {{-- @endforeach --}}
                                                            {{-- <li class="dropdown-divider"></li> --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_40') --}}
                                                            <a class="dropdown-item"
                                                                wire:click='ShowUpdate({{ $item->id }})'
                                                                href="javascript:void(0)"><i class="fas fa-edit"></i>
                                                                ແກ້ໄຂ</a>
                                                            {{-- @endif
                                                                @endforeach

                                                            @endif --}}
                                                            {{-- <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click="UpdateImport({{ $item->id }})"><i
                                                                    class="fas fa-pencil-alt">
                                                                    {{ __('lang.edit') }}</i></a> --}}
                                                            {{-- @if ($item->total_money - $item->total_paid != 0)
                                                                @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_38') --}}
                                                            @if ($item->total == $item->orders_logs_sum_total_paid)
                                                                <a class="dropdown-item" href="javascript:void(0)"
                                                                    wire:click="ShowPayment({{ $item->id }})"><i
                                                                        class="fas fa-history"></i>
                                                                    ປະຫັວດຊຳລະ</a>
                                                            @else
                                                                <a class="dropdown-item" href="javascript:void(0)"
                                                                    wire:click="ShowPayment({{ $item->id }})"><i
                                                                        class="fas fa-hand-holding-usd"></i>
                                                                    ຊຳລະຫນີ້</a>
                                                            @endif
                                                            {{-- @endif
                                                                @endforeach
                                                            @else --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_38') --}}
                                                            {{-- @endif
                                                                @endforeach --}}
                                                            {{-- @endif --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                @if ($items->ResFunctions->name == 'action_40') --}}
                                                            <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click="ShowBill({{ $item->id }})"><i
                                                                    class="fas fa-print">
                                                                </i> ພິມບິນ</a>
                                                            </a>
                                                            {{-- @endif
                                                            @endforeach --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                @if ($items->ResFunctions->name == 'action_40') --}}
                                                            {{-- <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click="ShowDetail({{ $item->id }})"><i
                                                                    class="fas fa-file"></i>
                                                                ລາຍລະອຽດ</a> --}}
                                                            </a>
                                                            {{-- @endif
                                                            @endforeach --}}
                                                            {{-- <li class="dropdown-divider"></li> --}}
                                                            {{-- <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click=""><i class="fas fa-times-circle">
                                                                    ຍົກເລີກ</i></a> --}}
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{ $data->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @endforeach
            <!-- /.modal-delete-->
    <div wire:ignore class="modal fabe" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash"> </i> ລຶບອອກ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ອອກບໍ່?</h3>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="Destroy({{ $ID }})" type="button" class="btn btn-success"><i
                            class="fa fa-trash"></i> ລຶບອອກ</button>
                </div>
            </div>
        </div>
    </div>
        {{-- \\\\\\\\\\\\\\\\\\\\\\\ Edit order item \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --}}
        <div class="modal fade" id="modal-update-item" wire:ignore.self>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fas fa-cart-plus"></i> ແກ້ໄຂໃບບິນ {{ $this->code }}
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row text-center">
                            <input type="hidden" wire:model="ID">
                            <div class="col-md-12">
                                <h4>ລາຍການສັ່ງຊື້</h4>
                            </div>
                        </div>
                        <table class="table table-hover text-center responsive">
                            <thead class="bg-light text-center">
                                <tr>
                                    <th>ລຳດັບ</th>
                                    <th>ສິນຄ້າ</th>
                                    <th>ຈຳນວນ</th>
                                    <th>ລາຄາ</th>
                                    <th>ເປັນເງິນ</th>
                                    <th>ຈັດການ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $num = 1;
                                @endphp
                                @foreach ($salesDetail as $item)
                                    <tr class="text-center">
                                        <td>{{ $num++ }}</td>
                                        <td>
                                            @if (!empty($item->product))
                                                {{ $item->product->name }}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input wire:model="stock.{{ $item->id }}" placeholder="0.00"
                                                            style="width: 100px" value="{{ $item->stock }}"
                                                            min="1" type="number"
                                                            class="form-control text-center money @error('stock.' . $item->id) is-invalid @enderror"
                                                            wire:change="UpdateStock({{ $item->id }})">
                                                        @error('stock.' . $item->id)
                                                            <span style="color: #ff0000"
                                                                class="error">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-left">
                                                    <div class="form-group">
                                                        {{ $item->stock }}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            {{ number_format($item->sell_price) }} ₭
                                        </td>
                                        <td>
                                            {{ number_format($item->subtotal) }} ₭
                                        </td>
                                        <td>
                                            <button wire:click="Remove_Item({{ $item->id }})"
                                                class="btn btn-danger btn-sm"><i class="fas fa-times-circle"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- \\\\\\\\\\\\\\\\\\\\\\\ show bill  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --}}
        <div class="modal fade" id="modal-bill" wire:ignore.self>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fas fa-file-alt"></i> ລາຍລະອຽດບິນ: {{ $this->code }}
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th>
                                            <span>
                                                @if (!empty($about))
                                                   <i class="fas fa-store-alt"></i> {{ $about->name_la }}
                                                @endif
                                            </span>
                                            <br>
                                            <span>
                                                @if (!empty($about))
                                                   ໂທ: {{ $about->phone }}
                                                @endif
                                            </span><br>
                                            <span>
                                                @if (!empty($about))
                                                    {{ $about->address }}
                                                @endif
                                            </span>
                                        </th>
    
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th>
                                            <span><i class="fas fa-user"></i> ລູກຄ້າ</span><br>
                                            <span>
                                                @if (!empty($customer_data))
                                                    {{ $customer_data->name_lastname }}
                                                @endif
                                            </span>
                                            <br>
                                            <span>
                                                @if (!empty($customer_data))
                                                   ໂທ: {{ $customer_data->phone }}
                                                @endif
                                            </span><br>
                                            <span>
                                                @if (!empty($customer_data))
                                                    {{ $customer_data->address }}
                                                @endif
                                            </span>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="right_content">
                        <div class="row text-center pt-3">
                            <input type="hidden" wire:model="ID">
                            <div class="col-md-12">
                                <h4><b>ໃບບິນການຂາຍ</b></h4>
                            </div>
                        </div>
                        <table class="table table-hover text-center responsive">
                            <thead class="bg-light text-center">
                                <tr>
                                    <th>ລຳດັບ</th>
                                    <th>ສິນຄ້າ</th>
                                    <th>ລາຄາ</th>
                                    <th>ຈຳນວນ</th>
                                    <th>ເປັນເງິນ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $num = 1;
                                @endphp
                                @foreach ($SalesDetail as $item)
                                    <tr class="text-center">
                                        <td>{{ $num++ }}</td>
                                        <td>
                                            @if (!empty($item->product))
                                                {{ $item->product->name }}
                                            @endif
                                        </td>
                                        <td>
                                            {{ number_format($item->sell_price) }} ₭
                                        </td>
                                        <td>
                                            x {{ $item->stock }}
                                        </td>
                                        <td>
                                            {{ number_format($item->subtotal) }} ₭
                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="text-bold bg-light">
                                    <td colspan="3">ຍອດລວມ</td>
                                    <td>x {{ number_format($this->sum_SalesDetail_stock) }}</td>
                                    <td>{{ number_format($this->sum_SalesDetail_subtotal) }} ₭</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-secondary fas fa-times-circle" data-dismiss="modal">
                            ປິດ</button>
                        <button id="print" type="button" class="btn btn-success"> <i class="fas fa-print"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div wire:ignore.self class="modal fabe" id="modal-onepay">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title"><i class="fas fa-credit-card"> </i> ຫຼັກຖານການໂອນ</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="{{ asset($this->onepay) }}" alt="" width="100%" height="400px">
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">ປິດ</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('livewire.backend.data-store.modal-script')
</div>
