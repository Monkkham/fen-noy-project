<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">ໜ້າຫຼັກ</a>
                    <a class="breadcrumb-item text-dark" href="#">ຮ້ານຄ້າ</a>
                    <span class="breadcrumb-item active">ລາຍການສິນຄ້າ</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->


    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <!-- Shop Sidebar Start -->
            {{-- <div class="col-lg-3 col-md-4">
              <!-- Price Start -->
              <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Filter by price</span></h5>
              <div class="bg-light p-4 mb-30">
                  <form>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" checked id="price-all">
                          <label class="custom-control-label" for="price-all">All Price</label>
                          <span class="badge border font-weight-normal">1000</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="price-1">
                          <label class="custom-control-label" for="price-1">$0 - $100</label>
                          <span class="badge border font-weight-normal">150</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="price-2">
                          <label class="custom-control-label" for="price-2">$100 - $200</label>
                          <span class="badge border font-weight-normal">295</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="price-3">
                          <label class="custom-control-label" for="price-3">$200 - $300</label>
                          <span class="badge border font-weight-normal">246</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="price-4">
                          <label class="custom-control-label" for="price-4">$300 - $400</label>
                          <span class="badge border font-weight-normal">145</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between">
                          <input type="checkbox" class="custom-control-input" id="price-5">
                          <label class="custom-control-label" for="price-5">$400 - $500</label>
                          <span class="badge border font-weight-normal">168</span>
                      </div>
                  </form>
              </div>
              <!-- Price End -->
              
              <!-- Color Start -->
              <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Filter by color</span></h5>
              <div class="bg-light p-4 mb-30">
                  <form>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" checked id="color-all">
                          <label class="custom-control-label" for="price-all">All Color</label>
                          <span class="badge border font-weight-normal">1000</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="color-1">
                          <label class="custom-control-label" for="color-1">Black</label>
                          <span class="badge border font-weight-normal">150</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="color-2">
                          <label class="custom-control-label" for="color-2">White</label>
                          <span class="badge border font-weight-normal">295</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="color-3">
                          <label class="custom-control-label" for="color-3">Red</label>
                          <span class="badge border font-weight-normal">246</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="color-4">
                          <label class="custom-control-label" for="color-4">Blue</label>
                          <span class="badge border font-weight-normal">145</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between">
                          <input type="checkbox" class="custom-control-input" id="color-5">
                          <label class="custom-control-label" for="color-5">Green</label>
                          <span class="badge border font-weight-normal">168</span>
                      </div>
                  </form>
              </div>
              <!-- Color End -->

              <!-- Size Start -->
              <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Filter by size</span></h5>
              <div class="bg-light p-4 mb-30">
                  <form>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" checked id="size-all">
                          <label class="custom-control-label" for="size-all">All Size</label>
                          <span class="badge border font-weight-normal">1000</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="size-1">
                          <label class="custom-control-label" for="size-1">XS</label>
                          <span class="badge border font-weight-normal">150</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="size-2">
                          <label class="custom-control-label" for="size-2">S</label>
                          <span class="badge border font-weight-normal">295</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="size-3">
                          <label class="custom-control-label" for="size-3">M</label>
                          <span class="badge border font-weight-normal">246</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                          <input type="checkbox" class="custom-control-input" id="size-4">
                          <label class="custom-control-label" for="size-4">L</label>
                          <span class="badge border font-weight-normal">145</span>
                      </div>
                      <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between">
                          <input type="checkbox" class="custom-control-input" id="size-5">
                          <label class="custom-control-label" for="size-5">XL</label>
                          <span class="badge border font-weight-normal">168</span>
                      </div>
                  </form>
              </div>
              <!-- Size End -->
          </div> --}}
            <!-- Shop Sidebar End -->


            <!-- Shop Product Start -->
            <div class="col-lg-12 col-md-12">
                <div class="row pb-3">
                    <div class="col-12 pb-1">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <div>
                                {{-- <button class="btn btn-sm btn-light"><i class="fa fa-th-large"></i></button>
                              <button class="btn btn-sm btn-light ml-2"><i class="fa fa-bars"></i></button> --}}
                            </div>
                            <div class="ml-2">
                                {{-- <div class="btn-group">
                                  <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">Sorting</button>
                                  <div class="dropdown-menu dropdown-menu-right">
                                      <a class="dropdown-item" href="#">Latest</a>
                                      <a class="dropdown-item" href="#">Popularity</a>
                                      <a class="dropdown-item" href="#">Best Rating</a>
                                  </div>
                              </div> --}}
                                {{-- <div class="btn-group ml-2">
                                  <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">ສະເເດງຫນ້າ</button>
                                  <div class="dropdown-menu dropdown-menu-right">
                                      <a class="dropdown-item" href="#">10</a>
                                      <a class="dropdown-item" href="#">20</a>
                                      <a class="dropdown-item" href="#">30</a>
                                  </div>
                              </div> --}}
                            </div>
                        </div>
                    </div>
                @if ($products->count() > 0)
                    @foreach ($products as $item)
                        <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                            <div class="product-item bg-light mb-4">
                                <div class="product-img position-relative overflow-hidden">
                                    <img class="img-fluid w-100" src="{{ asset($item->image) }}" alt="">
                                    <div class="product-action">
                                        <button wire:click='AddToCart({{ $item->id }})'
                                            class="btn btn-outline-dark btn-square"><i
                                                class="fa fa-shopping-cart"></i></button>
                                        <a wire:click='AddToWishList({{ $item->id }})'
                                            class="btn btn-outline-dark btn-square" href="#"><i
                                                class="far fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="text-center py-4">
                                    <a href="#" class="h6 text-decoration-none text-truncate"
                                        wire:click='ProductDetail({{ $item->id }})'>{{ $item->name }}
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5>{{ number_format($item->sell_price) }} ₭</h5>
                                            <h6 class="text-muted ml-2"><del>{{ number_format($item->buy_price) }}
                                                    ₭</del></h6>
                                        </div>
                                    </a>
                                    <div class="d-flex align-items-center justify-content-center mb-1">
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small>(99)</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @else
                    <style>
                        @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

                        body {
                            background-color: #eee;
                            font-family: 'Calibri', sans-serif !important;
                        }

                        .mt-100 {
                            margin-top: 10px;

                        }


                        .card {
                            margin-bottom: 30px;
                            border: 0;
                            -webkit-transition: all .3s ease;
                            transition: all .3s ease;
                            letter-spacing: .5px;
                            border-radius: 8px;
                            -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                            box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                        }

                        .card .card-header {
                            background-color: #fff;
                            border-bottom: none;
                            padding: 24px;
                            border-bottom: 1px solid #f6f7fb;
                            border-top-left-radius: 8px;
                            border-top-right-radius: 8px;
                        }

                        .card-header:first-child {
                            border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
                        }

                        .card .card-body {
                            padding: 30px;
                            background-color: transparent;
                        }
                    </style>
                    <div class="container-fluid text-center">
                        <div class="row">

                            <div class="col-md-12">

                                <div class="card">
                                    <div class="card-body cart">
                                        <div class="col-sm-12 empty-cart-cls text-center">
                                            <img src="https://cdn.dribbble.com/users/1785628/screenshots/5605512/media/097297f8e21d501ba45d7ce437ed77bd.gif"
                                                style="width: auto; height:200px; margin-left: 38%">
                                            <h3><strong><i class="fas fa-search"></i>
                                                    ບໍ່ພົບສິນຄ້າທີ່ທ່ານຄົ້ນຫາ!</strong></h3>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                @endif
                    {{-- <div class="col-12">
                        <nav>
                            <ul class="pagination justify-content-center">
                                <li class="page-item"><a class="page-link" href="#">
                                        {{ $products->links() }}
                                    </a></li>
                            </ul>
                        </nav>
                    </div> --}}
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
</div>
