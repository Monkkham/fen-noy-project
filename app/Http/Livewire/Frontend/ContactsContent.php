<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\Contacts;

class ContactsContent extends Component
{
    public function render()
    {
        return view('livewire.frontend.contacts-content')->layout('layouts.frontend.style');
    }
    public $name, $phone, $subject, $message,$about;
    public function resetform()
    {
        $this->name = '';
        $this->phone = '';
        $this->subject = '';
        $this->message = '';
    }
    public function SendMessage()
    {
        $this->validate([
            'name' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ], [
            'name.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'subject.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'message.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $data = new Contacts();
        $data->name = $this->name;
        $data->phone = $this->phone;
        $data->subject = $this->subject;
        $data->message = $this->message;
        $data->save();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        $this->resetform();
    }
}
