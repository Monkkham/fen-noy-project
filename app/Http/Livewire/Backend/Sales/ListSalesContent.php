<?php

namespace App\Http\Livewire\Backend\Sales;

use App\Models\Sales;
use App\Models\SalesDetail;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class ListSalesContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search, $start_date, $end_date, $status,$onepay, $ID, $caculate, $SalesDetail = [], $sum_SalesDetail_subtotal, $sum_SalesDetail_stock, $customer_data;

    public function render()
    {
        $end = date('Y-m-d H:i:s', strtotime($this->end_date . '23:23:59'));
        $data = Sales::withSum('sales_detail', 'subtotal')->where(function ($query) {
            $query->where('code', 'like', '%' . $this->search . '%');
        });
        if ($this->start_date && $this->end_date) {
            $data = $data->whereBetween('created_at', [$this->start_date, $end]);
        }
        if ($this->status) {
            $data = $data->where('status', $this->status);
        }
        if (!empty($data)) {
            $data = $data->paginate(10);
        } else {
            $data = [];
        }
        return view('livewire.backend.sales.list-sales-content', compact('data'))->layout('layouts.backend.style');
    }
    public function resetField()
    {
        $this->ID = '';
    }
    public function showDestory($ids)
    {
        $this->ID = $ids;
        $data = Sales::find($ids);
        $this->dispatchBrowserEvent('show-modal-delete');
    }
    public function Destroy()
    {
        $data = Sales::find($this->ID);
        $data->delete();
        $this->resetField();
        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
    }
    public $salesDetail = [], $stock = [], $code;
    public function ShowUpdate($ids)
    {
        $this->dispatchBrowserEvent('show-modal-update-item');
        $sales = Sales::find($ids);
        $this->ID = $sales->id;
        $this->code = $sales->code;
        $this->salesDetail = SalesDetail::where('sales_id', $this->ID)->get();
        $this->stock = $this->salesDetail->pluck('stock');
    }

    public function Remove_Item($id)
    {
        $salesDetail = SalesDetail::find($id);
        $salesDetail->delete();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon' => 'success',
        ]);
    }

    public function UpdateStock($id)
    {
        $salesDetail = SalesDetail::find($id);
        $salesDetail->stock = $this->stock[$id];
        $salesDetail->subtotal = $salesDetail->sell_price * $this->stock[$id];
        $salesDetail->save();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຈຳນວນສຳເລັດ!',
            'icon' => 'success',
        ]);
    }

    public function ShowBill($id)
    {
        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-bill');
        $sales = Sales::find($id);
        $this->ID = $sales->id;
        $this->customer_data = $sales->customer ?? '';
        $this->code = $sales->code;
        $this->sum_SalesDetail_subtotal = SalesDetail::select('subtotal')->where('sales_id', $this->ID)->sum('subtotal');
        $this->sum_SalesDetail_stock = SalesDetail::select('stock')->where('sales_id', $this->ID)->sum('stock');
        $this->SalesDetail = SalesDetail::where('sales_id', $this->ID)->get();
    }
    public function showOnepay($ids)
    {
        $this->ID = $ids;
        $data = Sales::find($ids);
        $this->onepay = $data->onepay;
        $this->dispatchBrowserEvent('show-modal-onepay');
    }
}
