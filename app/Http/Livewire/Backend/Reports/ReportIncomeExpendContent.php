<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\Sales;
use App\Models\Orders;
use Livewire\Component;
use App\Models\IncomeExpend;

class ReportIncomeExpendContent extends Component
{
    public $start_date, $end_date, $type;
    public function mount()
    {
        $this->start_date = date('Y-m-d');
        $this->end_date = date('Y-m-d');
    }
    public function render()
    {
        $end = date('Y-m-d H:i:s', strtotime($this->end_date . '23:23:59'));
        if ($this->start_date && $this->end_date) {
            $sales = Sales::whereBetween('created_at', [$this->start_date, $end])->get();
            $orders = Orders::whereBetween('created_at', [$this->start_date, $end])->get();

            $sum_money_income = IncomeExpend::whereBetween('created_at', [$this->start_date, $end])->where('type',1)->sum('total_price');
            $sum_money_expend = IncomeExpend::whereBetween('created_at', [$this->start_date, $end])->where('type',2)->sum('total_price');
        } else {
            $sales = [];
            $orders = [];
        }
        $sum_total_income = $sales->sum('total');
        $sum_total_expend = $orders->sum('total');
        // if ($this->type) {
        //     $sales = $sales->where('type', $this->type);
        //     $sum_total_income = $sales->where('type', $this->type)->sum('total');
        // }
        return view('livewire.backend.reports.reports-income-expend-content',compact('sales','orders','sum_total_income','sum_total_expend','sum_money_income','sum_money_expend'))->layout('layouts.backend.style');
    }
}
