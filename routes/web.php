<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Backend\AboutContent;
use App\Http\Livewire\Backend\LoginContent;
use App\Http\Livewire\Backend\RolesContent;
use App\Http\Livewire\Backend\SlideContent;
use App\Http\Livewire\Frontend\HomeContent;
use App\Http\Livewire\Backend\LogoutContent;
use App\Http\Livewire\Backend\ProfileContent;
use App\Http\Livewire\Backend\VillageContent;
use App\Http\Livewire\Frontend\AboutsContent;
use App\Http\Livewire\Frontend\SearchContent;
use App\Http\Livewire\Backend\DistrictContent;
use App\Http\Livewire\Backend\ProvinceContent;
use App\Http\Livewire\Backend\DashboardContent;
use App\Http\Livewire\Frontend\ContactsContent;
use App\Http\Livewire\Backend\Sales\SalesContent;
use App\Http\Livewire\Backend\IncomeExpendContent;
use App\Http\Livewire\Backend\Orders\ImportContent;
use App\Http\Livewire\Backend\Orders\OrdersContent;
use App\Http\Livewire\Backend\DataStore\UserContent;
use App\Http\Livewire\Frontend\ProductDetailContent;
use App\Http\Livewire\Backend\DataStore\UnitsContent;
use App\Http\Livewire\Backend\Sales\ListSalesContent;
use App\Http\Livewire\Backend\Reports\ProductsContent;
use App\Http\Livewire\Backend\DataStore\ProductContent;
use App\Http\Livewire\Backend\Orders\OrdersCartContent;
use App\Http\Livewire\Backend\Orders\ImportUpdateContent;
use App\Http\Livewire\Backend\Reports\SalesReportsContent;
use App\Http\Livewire\Backend\DataStore\ProductTypeContent;
use App\Http\Livewire\Backend\Reports\OrdersReportsContent;
use App\Http\Livewire\Backend\Reports\ReportIncomeExpendContent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });

// ========== Front-end ========== //
Route::get('/', HomeContent::class)->name('frontend.home');
Route::get('/aboutss', AboutsContent::class)->name('frontend.abouts');
Route::get('/contactss', ContactsContent::class)->name('frontend.contacts');
Route::get('/Searchs', SearchContent::class)->name('frontend.Search');
Route::get('/ProductDetail/{slug_id}', ProductDetailContent::class)->name('frontend.ProductDetails');
// Route::middleware('auth.frontend')->group(function () {
//     Route::get('/logouts', [LogoutContent::class, 'logout'])->name('frontend.logout');
//     Route::get('/profiles/{id}', ProfileContent::class)->name('frontend.profile');
// });

// ========== Backend ====================================//
Route::get('/admin-login', LoginContent::class)->name('backend.login');


Route::group(['middleware' => 'auth.backend'], function () {
    Route::get('/logout', [LogoutContent::class, 'logout'])->name('backend.logout');
    Route::get('/dashboard', DashboardContent::class)->name('backend.dashboard');
    Route::get('/admin-profiles', ProfileContent::class)->name('backend.profile');
    Route::get('/users', UserContent::class)->name('backend.user');
    Route::get('/roles', RolesContent::class)->name('backend.role');
    Route::get('/villages', VillageContent::class)->name('backend.village');
    Route::get('/districts', DistrictContent::class)->name('backend.district');
    Route::get('/provinces', ProvinceContent::class)->name('backend.province');
    Route::get('/abouts', AboutContent::class)->name('backend.about');
    Route::get('/slides', SlideContent::class)->name('backend.slide');

    Route::get('/product_types', ProductTypeContent::class)->name('backend.product_type');
    Route::get('/units', UnitsContent::class)->name('backend.unit');
    Route::get('/products', ProductContent::class)->name('backend.product');

    Route::get('/Orders', OrdersContent::class)->name('backend.order');
    Route::get('/OrderCarts', OrdersCartContent::class)->name('backend.OrderCart');
    Route::get('/OrderImports', ImportContent::class)->name('backend.OrderImport');
    Route::get('/imports-update/{slug_id}', ImportUpdateContent::class)->name('backend.import_update');
    Route::get('/Sales', SalesContent::class)->name('backend.sale');
    Route::get('/ListSales', ListSalesContent::class)->name('backend.ListSale');

    Route::get('/SalesReports', SalesReportsContent::class)->name('backend.SalesReport');
    Route::get('/OrdersReports', OrdersReportsContent::class)->name('backend.OrdersReport');
    Route::get('/ProductsReports', ProductsContent::class)->name('backend.ProductsReport');
    Route::get('/IncomeExpendContents', IncomeExpendContent::class)->name('backend.IncomeExpendContent');
    Route::get('/ReportIncomeExpends', ReportIncomeExpendContent::class)->name('backend.ReportIncomeExpend');

    

});
